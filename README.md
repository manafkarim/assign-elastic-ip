# Auto Assign Elastic IP

Script to assign Elastic IP to an instance from the tagged Elastic IP pool.

# Prerequisites

A pool of IPs with a tag named '**env**' and with value of (*here*) '**prod**'.
You will need to have at minimum the same number of IP addresses allocated as you will have instances, but if possible we recommend having at least one additional spare. 
An Instance profile with the necessary permission to manage EC2 and IPs
```
{
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:AssociateAddress",
        "ec2:DescribeAddresses",
        "ec2:DescribeTags",
        "ec2:DescribeInstances"
      ],
      "Resource": "*"
    }
  ]
}
```


# User Data script code

```
#!/bin/bash

git clone https://gitlab.com/manafkarim/assign-elastic-ip.git

chmod +rx ~/auto-assign-elastic-ip.sh

~/auto-assign-elastic-ip.sh
```


The instances will need to be started with a matching Environment tag as your IP pool, otherwise it will fail to find any addresses to allocate. In our example above, you would need to add the tag Environment=Production.

Once everything is setup, your instances will have an Elastic IP automatically assigned on start up.

